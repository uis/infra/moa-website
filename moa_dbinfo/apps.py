from django.apps import AppConfig


class MoaDbinfoConfig(AppConfig):
    name = 'moa_dbinfo'
