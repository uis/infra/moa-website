from django.db import models

# This database is held on Moa, and Moa reserves names beginning with
# "moa_" for its own use, so we need to be a little careful with table
# naming.

class Cluster(models.Model):
    cluster = models.SlugField(primary_key=True)
    ca_certificate = models.TextField()
    postgres_password = models.CharField(max_length=64)
    bootstrap_admin = models.ManyToManyField('auth.User')
    oook_target_host = models.CharField(max_length=64, blank=True)
    oook_target_fs = models.CharField(max_length=1024, blank=True)
    oook_ssh_pubkeys = models.CharField(max_length=10240, blank=True)
    class Meta:
        db_table = 'metamoa_cluster'

class ClusterFQDN(models.Model):
    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE)
    fqdn = models.CharField(max_length=64)
    class Meta:
        db_table = 'metamoa_clusterfqdn'
