from django.views.generic import DetailView, ListView
from django.http import HttpResponse
from .models import Cluster

import json

class ClusterList(ListView):
    model = Cluster

from django.views.generic.list import BaseListView

class ClusterBackupList(BaseListView):
    model = Cluster
    def render_to_response(self, ctx):
        j = [{ 'target_host': cluster.oook_target_host,
               'target_fses': ["/etc", cluster.oook_target_fs],
               'ssh_pubkeys': cluster.oook_ssh_pubkeys }
             for cluster in self.object_list if cluster.oook_target_host]
        return HttpResponse(json.dumps(j), content_type="application/json")

from django.contrib.auth.mixins import UserPassesTestMixin

class ClusterDetail(UserPassesTestMixin, DetailView):
    model = Cluster
    def test_func(self):
        # We can't use self.object here because it hasn't been fetched
        # yet, so we fetch it ourselves.
        return self.request.user in self.get_object().bootstrap_admin.all()

from django.views.generic.detail import BaseDetailView

class ClusterCACert(BaseDetailView):
    model = Cluster
    def render_to_response(self, ctx):
        return HttpResponse(self.object.ca_certificate,
                            content_type='application/x-x509-ca-cert')
