from django.urls import path
from . import views

urlpatterns = [
    path('', views.ClusterList.as_view()),
    path('oook.json', views.ClusterBackupList.as_view()),
    path('<slug:pk>/', views.ClusterDetail.as_view()),
    path('<slug:pk>/ca.crt', views.ClusterCACert.as_view()),
]
