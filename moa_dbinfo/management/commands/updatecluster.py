from django.core.management.base import BaseCommand
from django.db import transaction
from moa_dbinfo.models import Cluster
from django.contrib.auth.models import User

class Command(BaseCommand):
    help = 'Creates a record for a new cluster'

    def add_arguments(self, parser):
        parser.add_argument("cluster")
        parser.add_argument("password")
        parser.add_argument("cacert")
        parser.add_argument("oook_target_host", nargs='?')
        parser.add_argument("oook_target_fs", nargs='?')
        parser.add_argument("oook_ssh_pubkeys", nargs='?')
        parser.add_argument("--fqdn", action='append')
        parser.add_argument("--admin", action='append')

    @transaction.atomic
    def handle(self, cluster, password, cacert, oook_target_host,
               oook_target_fs, oook_ssh_pubkeys, fqdn, admin, **kwargs):
        # This command is run by Ansible, so it needs to detect if
        # anything is changed.
        changed = False
        admin_users = set(User.objects.filter(username__in=admin))
        for missing in set(admin) - { u.username for u in admin_users }:
            admin_users.add(User.objects.create_user(missing))
        clusters = Cluster.objects.filter(pk=cluster)
        if len(clusters) == 0:
            cluster_object = Cluster(pk=cluster)
            changed = True
        else:
            cluster_object = clusters[0]
        if cluster_object.postgres_password != password:
            cluster_object.postgres_password = password
            changed = True
        if cluster_object.ca_certificate != cacert:
            cluster_object.ca_certificate = cacert
            changed = True
        if (oook_target_host != None and
            cluster_object.oook_target_host != oook_target_host):
            cluster_object.oook_target_host = oook_target_host
            changed = True
        if (oook_target_fs != None and
            cluster_object.oook_target_fs != oook_target_fs):
            cluster_object.oook_target_fs = oook_target_fs
            changed = True
        if (oook_ssh_pubkeys != None and
            cluster_object.oook_ssh_pubkeys != oook_ssh_pubkeys):
            cluster_object.oook_ssh_pubkeys = oook_ssh_pubkeys
            changed = True
        oldfqdns = {f.fqdn for f in cluster_object.clusterfqdn_set.all()}
        newfqdns = set(fqdn)
        if newfqdns != oldfqdns:
            (cluster_object.clusterfqdn_set.exclude(fqdn__in=newfqdns)
             .delete())
            for f in newfqdns - oldfqdns:
                cluster_object.clusterfqdn_set.create(fqdn = f)
            changed = True
        if (admin_users != set(cluster_object.bootstrap_admin.all())):
            cluster_object.bootstrap_admin.set(admin_users)
            changed = True
        if changed: cluster_object.save()
